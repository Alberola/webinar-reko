import boto3
import json

def get_text(image, bucket):
    
    client=boto3.client('rekognition','us-west-2')
    
    response=client.detect_text(Image={'S3Object':{'Bucket':bucket,'Name':image}})
    
    textDetections=response['TextDetections']

    print(json.dumps(response,indent=4))

    print ('Detected text\n----------')
    for t in response['TextDetections']:
        print(t['DetectedText'] + ':' + str(t['Confidence']))
    
    print ('\n\nMore info\n----------')
    for text in textDetections:
            print ('Detected text:' + text['DetectedText'])
            print ('Confidence: ' + "{:.2f}".format(text['Confidence']) + "%")
            print ('Type:' + text['Type'])
            print()
    return len(textDetections)

    
def main():

    bucket='webinar-reko'
    image='cartel.jpeg'
    text_count=get_text(image,bucket)
    print("Text detected: " + str(text_count))


if __name__ == "__main__":
    main()    

    
