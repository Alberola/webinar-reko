import boto3
import json

def compare_faces(source, target, bucket):

    client=boto3.client("rekognition","us-west-2")

    response=client.compare_faces(SimilarityThreshold=1,
                                  SourceImage={'S3Object':{'Bucket':bucket,'Name':source}},
                                  TargetImage={'S3Object':{'Bucket':bucket,'Name':target}})

    print(json.dumps(response,indent=4))
    
    for faceMatch in response['FaceMatches']:
        position = faceMatch['Face']['BoundingBox']
        similarity = str(faceMatch['Similarity'])
        print('The face at ' +
               str(position['Left']) + ' ' +
               str(position['Top']) +
               ' matches with ' + similarity + '% confidence')

    return len(response['FaceMatches'])

def main():
    bucket="webinar-reko"
    source_file='parejo3.png'
    target_file='gaya1.png'    
    face_matches=compare_faces(source_file, target_file, bucket)
    print("Face matches: " + str(face_matches))

if __name__ == "__main__":
    main()

