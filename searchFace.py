import boto3
import json

bucket="webinar-reko"
collectionId="futbolistas"
fileName='parejo3.png'
threshold = 1
maxFaces=3

client=boto3.client("rekognition","us-west-2")
  
response=client.search_faces_by_image(CollectionId=collectionId,
                            Image={'S3Object':{'Bucket':bucket,'Name':fileName}},
                            FaceMatchThreshold=threshold,
                            MaxFaces=maxFaces)

print(json.dumps(response,indent=4))

faceMatches=response['FaceMatches']
print ('Matching faces')
for match in faceMatches:
    print ('FaceId:'  + match['Face']['ExternalImageId'])
    print ('Similarity: ' + "{:.2f}".format(match['Similarity']) + "%")
    print
    
    

