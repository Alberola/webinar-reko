import boto3
import json

def detect_labels(photo, bucket):

    client=boto3.client('rekognition','us-west-2')

    response = client.detect_labels(Image={'S3Object':{'Bucket':bucket,'Name':photo}})
  
    print('Detected labels for ' + photo)

    print(json.dumps(response,indent=4))
    
    for label in response['Labels']:
        print ("Label: " + label['Name'])
        print ("Confidence: " + str(label['Confidence']))

    return len(response['Labels'])


def main():
    bucket="webinar-reko"
    image="computer.jpg"
    label_count=detect_labels(image, bucket)
    print("Labels detected: " + str(label_count))
    print()   


if __name__ == "__main__":
    main()


